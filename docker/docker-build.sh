#!/usr/bin/env bash

version=$1

cp -R ../dist .
docker build -t registry.gitlab.com/maniekq/weatherstation-ui:${version} .
rm -rf dist
