#!/usr/bin/env bash

version=$1

docker push registry.gitlab.com/maniekq/weatherstation-ui:${version}
