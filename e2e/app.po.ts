export class UiAppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('ui-app-app h1')).getText();
  }
}
