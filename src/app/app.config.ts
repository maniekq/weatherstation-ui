import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
  public indoorDataUrl: string = "http://192.168.0.170:9091/indoor";
  public indoorHistoricalDataUrl: string = "http://192.168.0.170:9091/indoor/history?period=24h";

  public outdoorDataUrl: string = "http://192.168.0.170:9091/outdoor";
  public outdoorForecastDataUrl: string = "http://192.168.0.170:9091/outdoor/forecast";

  public wroclawCoordinates: Coordinates = new Coordinates(51.095734, 16.988525, 'Wrocław');
  private londonCoordinates: Coordinates = new Coordinates(51.505624, -0.300216, 'London');
  private colomboCoordinates: Coordinates = new Coordinates(6.937071, 79.842824, 'Colombo');
  private newYorkCoordinates: Coordinates = new Coordinates(40.743628, -73.958707, 'New York');
  private lisbonCoordinates: Coordinates = new Coordinates(38.728783, -9.150238, 'Lisbon');
  private balaklavaCoordinates: Coordinates = new Coordinates(44.505709, 33.599045, 'Balaklava');
  private antwerpCoordinates: Coordinates = new Coordinates(51.209946, 4.397650, 'Antwerp');

  public cities: Coordinates[] = [this.londonCoordinates, this.colomboCoordinates, this.newYorkCoordinates,
    this.lisbonCoordinates, this.balaklavaCoordinates, this.antwerpCoordinates];


  // public indoorDataUrl: string = "http://localhost:8080/indoor";
  // public indoorHistoricalDataUrl: string = "http://localhost:8080/indoor/history?period=24h";
  //
  // public outdoorDataUrl: string = "http://localhost:8080/outdoor";
  // public outdoorForecastDataUrl: string = "http://localhost:8080/outdoor/forecast?period=24h";
}

class Coordinates {
  constructor(public latitude: number, public longitude: number, public name: string) {}
}
