import {Component, OnInit, AfterViewInit, Input, SimpleChange} from '@angular/core';
import * as c3 from 'c3';

@Component({
  moduleId: module.id,
  selector: 'wth-area-chart',
  templateUrl: 'area-chart.component.html',
  styleUrls: ['area-chart.component.css']
})
export class AreaChartComponent implements OnInit, AfterViewInit {

  @Input() data: c3.Data;
  @Input() labels: Array<string>;
  chartId: string;
  chart: c3.ChartAPI;

  constructor() {}

  ngOnInit(): any {
    this.chartId = `chart-${AreaChartComponent.guid()}`;
  }

  ngOnChanges(changes: {[value: string]: SimpleChange}) {
    if (changes['data'] && this.chart) {
      this.chart.load({
        columns: this.data.columns
      })
    }
  }

  ngAfterViewInit(): any {


    this.chart = c3.generate({
      bindto: `#${this.chartId}`,
      data: this.data,
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%H'
          },
          padding: {
            left: 0,
            right: 0,
          }
        },
        y: {
          label: this.labels[0],
          min: 15,
          padding: {
            top: 15,
            bottom: 0
          }
        },
        y2: {
          label: this.labels[1],
          min: 30,
          padding: {
            top: 20,
            bottom: 0
          },
          show: true
        }
      },
      legend: {
        show: false
      },
      tooltip: {
        show: false
      },
      padding: {
        top: 20,
        right: 25,
        left: 20
      }
    });
  }

  static guid() {
    return AreaChartComponent.s4() + AreaChartComponent.s4() + '-' + AreaChartComponent.s4() + '-' + AreaChartComponent.s4() + '-' +
      AreaChartComponent.s4() + '-' + AreaChartComponent.s4() + AreaChartComponent.s4() + AreaChartComponent.s4();
  }

  private static s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

}
