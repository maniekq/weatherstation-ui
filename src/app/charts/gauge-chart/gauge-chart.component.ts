import {Component, OnInit, Input, AfterViewInit, SimpleChange} from '@angular/core';
import * as c3 from 'c3';

@Component({
  moduleId: module.id,
  selector: 'wth-gauge-chart',
  templateUrl: 'gauge-chart.component.html',
  styleUrls: ['gauge-chart.component.css']
})
export class GaugeChartComponent implements OnInit, AfterViewInit {

  @Input() value: number;
  @Input() label: string;
  @Input() unit: string;
  @Input() unitLabel: string;
  @Input() colors: {[a :number]: string};
  @Input() min: number;
  @Input() max: number;

  chartId: string;
  chart: c3.ChartAPI;

  ngOnChanges(changes: {[value: string]: SimpleChange}) {
    if (changes['value'] && this.chart) {
      this.chart.load({
        columns: [
          ['data', this.value]
        ]
      })
    }

  }

  ngAfterViewInit(): any {

    var colorsValues: string[] = [];
    var colorsKeys: string[] = [];
    for(var key in this.colors) {
      if (key != '0') {
        colorsKeys.push(key);
      }
      colorsValues.push(this.colors[key])
    }

    this.chart= c3.generate({
      bindto: `#${this.chartId}`,
      data: {
        columns: [
          ['data', this.value]
        ],
        type: 'gauge'
      },
      gauge: {
        label: {
          format: function (value, ratio) {
            return value;
          },
          show: true // to turn off the min/max labels.
        },
        min: this.min,
        max: this.max,
        units: this.unitLabel,
        width: 50, // for adjusting arc thickness
        expand: false
      },
      color: {
        pattern: colorsValues,
        threshold: {
          unit: this.unit, // percentage is default
          max: this.max, // 100 is default
          values: colorsKeys
        }
      },
      padding: {
        top: 0,
        right: 0,
        left: 0
      },
      tooltip: {
        show: false
      }
    });
  }


  ngOnInit(): any {
    this.chartId = `chart-${this.label}-${GaugeChartComponent.guid()}`;
  }

  static guid() {
    return GaugeChartComponent.s4() + GaugeChartComponent.s4() + '-' + GaugeChartComponent.s4() + '-' + GaugeChartComponent.s4() + '-' +
      GaugeChartComponent.s4() + '-' + GaugeChartComponent.s4() + GaugeChartComponent.s4() + GaugeChartComponent.s4();
  }

  private static s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

}
