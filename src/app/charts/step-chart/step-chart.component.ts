
import {Component, Input, OnInit, AfterViewInit, SimpleChange} from '@angular/core';
import * as c3 from 'c3';


@Component({
  moduleId: module.id,
  selector: 'wth-step-chart',
  templateUrl: 'step-chart.component.html',
  styleUrls: ['step-chart.component.css']
})
export class StepChartComponent implements OnInit, AfterViewInit {

  @Input() data: c3.Data;
  @Input() label: string;

  chartId: string;
  chart: c3.ChartAPI;

  ngOnChanges(changes: {[value: string]: SimpleChange}) {
    if (changes['data'] && this.chart) {
      this.chart.load({
        columns: this.data.columns
      })
    }
  }

  ngAfterViewInit(): any {

    this.chart = c3.generate({
      bindto: `#${this.chartId}`,
      data: this.data,
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%H:%M'
          },
          padding: {
            left: 0,
            right: 0,
          }
        },
        y: {
          label: this.label,
          padding: {
            top: 0,
            bottom: 0
          }
        }
      },
      legend: {
        show: false
      },
      tooltip: {
        show: false
      },
      padding: {
        top: 20,
        right: 20,
        left: 25
      },
    });
  }



  ngOnInit(): any {
    this.chartId = `chart-${this.label}`;

  }


}
