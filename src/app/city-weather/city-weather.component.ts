import {Component, Input} from '@angular/core';
import {WeatherIconComponent} from "../weather-icon/weather-icon.component";
import {WeatherIcon} from "../service/data";

@Component({
  moduleId: module.id,
  selector: 'wth-city-weather',
  templateUrl: 'city-weather.component.html',
  styleUrls: ['city-weather.component.css'],
  directives: [WeatherIconComponent]
})
export class CityWeatherComponent {

  @Input() icon: WeatherIcon;
  @Input() name: string;
  @Input() temperature: number;

  constructor() {}

}
