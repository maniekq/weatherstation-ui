import {Component, OnInit, Input, SimpleChange} from '@angular/core';
import * as moment from 'moment';


@Component({
  moduleId: module.id,
  selector: 'wth-clock',
  templateUrl: 'clock.component.html',
  styleUrls: ['clock.component.css']
})
export class ClockComponent implements OnInit {
  @Input() datetime: Date;


  formattedTime: string;
  dayOfWeek: string;
  dayOfMonth: string;

  constructor() {

  }

  ngOnInit(): any {
    this.formatTime(this.datetime);
  }

  ngOnChanges(changes: {[value: string]: SimpleChange}) {
    console.log("time change: ", this.datetime);
    if (changes['datetime']) {
      this.formatTime(this.datetime);
    }
  }

  formatTime(datetime: Date) {
    this.formattedTime = moment(datetime).format('HH:mm');
    this.dayOfWeek = moment(datetime).format('ddd');
    this.dayOfMonth = moment(datetime).format('D');
  }


}
