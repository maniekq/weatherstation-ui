import {Component, Input} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'wth-outdoor-weather',
  templateUrl: 'outdoor-weather.component.html',
  styleUrls: ['outdoor-weather.component.css']
})
export class OutdoorWeatherComponent  {

  @Input() temperature: number;
  @Input() pressure: number;
  @Input() windSpeed: number;

  constructor() {}

}
