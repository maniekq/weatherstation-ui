import { Injectable } from '@angular/core';
import {Configuration} from "../app.config";
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Observable';
import {IndoorData, OutdoorData, WeatherIcon} from "./data";

@Injectable()
export class DataService {
  private headers: Headers;
  private _configuration: Configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  public getIndoorData = (): Observable<IndoorData> => {
    return this._http.get(this._configuration.indoorDataUrl)
      .map((response: Response) => <IndoorData>response.json());
  };

  public getIndoorHistoricalData = (): Observable<IndoorData[]> => {
    return this._http.get(this._configuration.indoorHistoricalDataUrl)
      .map((response: Response) => <IndoorData[]>response.json());
  };

  public getOutdoorCurrentData = (latitude: number, longitude: number): Observable<OutdoorData> => {
    let params = new URLSearchParams();
    params.set('latitude', latitude.toString());
    params.set('longitude', longitude.toString());
    return this._http.get(this._configuration.outdoorDataUrl, {search: params})
      .map((response: Response) => {
        let outdoorData: OutdoorData = <OutdoorData>response.json();
        console.log("JSON....: ", response.json());
        outdoorData.icon = (<any>WeatherIcon)[outdoorData.icon];
        return outdoorData;
      });
  };

  public getOutdoorForecastData = (latitude: number, longitude: number): Observable<OutdoorData[]> => {
    let params = new URLSearchParams();
    params.set('latitude', latitude.toString());
    params.set('longitude', longitude.toString());
    params.set('period', '12h');
    return this._http.get(this._configuration.outdoorForecastDataUrl, {search: params})
      .map((response: Response) => <OutdoorData[]>response.json());
  };

}
