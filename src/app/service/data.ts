export class IndoorData {

  constructor(public temperature: number,
              public humidity: number,
              public date: number) {
  }
}

export class OutdoorData {


  constructor(public icon: WeatherIcon,
              public temperature: number,
              public pressure: number,
              public windSpeed: number,
              public precipitationProbability: number,
              public time: number) {
  }
}

export enum WeatherIcon {
  ClearDay, ClearNight, Rain, Snow, Sleet, Wind, Fog, Cloudy, PartlyCloudyDay, PartlyCloudyNight, Other
}
