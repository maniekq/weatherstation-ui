import {Component, OnInit} from "@angular/core";
import {WeatherIconComponent} from "./weather-icon";
import {OutdoorWeatherComponent} from "./outdoor-weather";
import {StepChartComponent} from "./charts/step-chart";
import {CityWeatherComponent} from "./city-weather/city-weather.component";
import {GaugeChartComponent} from "./charts/gauge-chart/gauge-chart.component";
import {AreaChartComponent} from "./charts/area-chart/area-chart.component";
import {ClockComponent} from "./clock/clock.component";
import {IndoorData, OutdoorData, WeatherIcon} from "./service/data";
import * as c3 from "c3";
import {DataService} from "./service/data.service";
import {Configuration} from "./app.config";

@Component({
  moduleId: module.id,
  selector: 'ui-app-app',
  templateUrl: 'ui-app.component.html',
  styleUrls: ['ui-app.component.css'],
  providers: [DataService],
  directives: [WeatherIconComponent, OutdoorWeatherComponent, StepChartComponent, CityWeatherComponent,
    GaugeChartComponent, AreaChartComponent, ClockComponent]
})


export class UiAppAppComponent implements OnInit {

  private configuration = new Configuration();

  ngOnInit(): any {
    this.refreshIndoorData();
    this.refreshIndoorHistoricalData();
    this.refreshClock();
    this.refreshOutdoorData();
    this.refreshOutdoorForecastData();
    this.refreshCitiesData();
  }

  refreshIndoorData() {
    this.dataService.getIndoorData().subscribe(
      (data: IndoorData)=> {
        this.indoorData = data;
      },
      error => console.log(error)
    );

    setTimeout(() => this.refreshIndoorData(), 15000);
  }

  refreshIndoorHistoricalData() {
    this.dataService.getIndoorHistoricalData().subscribe(
      (data: IndoorData[])=> {
        this.indoorHistoryChartData = this.prepareIndoorHistoryChartData(data);
      },
      error => {
        console.log(error);
      }
    );
    setTimeout(() => this.refreshIndoorHistoricalData(), 15000);
  }

  refreshClock() {
    this.currentTime = new Date(Date.now());
    setTimeout(() => this.refreshClock(), 30 * 1000)
  }

  refreshOutdoorData() {
    let coords = this.configuration.wroclawCoordinates;
    this.dataService.getOutdoorCurrentData(coords.latitude, coords.longitude).subscribe(
      (data: OutdoorData)=> {
        console.log(data);
        console.log("data.icon: ", data.icon);
        this.outdoorWeather = data;
      },
      error => {
        console.log(error);
      }
    );
    setTimeout(() => this.refreshOutdoorData(), 30 * 60 * 1000);
  }

  refreshOutdoorForecastData() {
    let coords = this.configuration.wroclawCoordinates;
    this.dataService.getOutdoorForecastData(coords.latitude, coords.longitude).subscribe(
      (data: OutdoorData[])=> {
        console.log(data);
        this.temperatureForecastChartData = this.prepareTemperatureForecastChartData(data);
        this.precipitationForecastChartData = this.preparePrecipitationForecastChartData(data);
      },
      error => {
        console.log(error);
      }
    );
    setTimeout(() => this.refreshOutdoorForecastData(), 30 * 60 * 1000);
  }

  refreshCitiesData() {
    this.configuration.cities.forEach((coordinates) => {
      this.dataService.getOutdoorCurrentData(coordinates.latitude, coordinates.longitude).subscribe(
        (data: OutdoorData)=> {
          console.log(data);
          this.cities.find((city) => city.cityName === coordinates.name).weather = data;
        },
        error => {
          console.log(error);
        }
      );
    });

    setTimeout(() => this.refreshCitiesData(), 30 * 60 * 1000);
  }

  settings: Settings = new Settings();
  outdoorWeather: OutdoorData = new OutdoorData(WeatherIcon.PartlyCloudyDay, -100.0, 1016.5, 5.7, 13.2, 1479747893093);
  temperatureForecastChartData: c3.Data = this.prepareTemperatureForecastChartData([]);
  precipitationForecastChartData: c3.Data = this.preparePrecipitationForecastChartData([]);

  cities: CityWeather[] = [
    new CityWeather("London", new OutdoorData(WeatherIcon.Rain, 16, 1004, 10, 5, 1479747893093)),
    new CityWeather("Colombo", new OutdoorData(WeatherIcon.ClearDay, 35, 1004, 10, 5, 1479747893093)),
    new CityWeather("New York", new OutdoorData(WeatherIcon.Sleet, 16, 1004, 10, 5, 1479747893093)),
    new CityWeather("Lisbon", new OutdoorData(WeatherIcon.Wind, 24, 1004, 10, 5, 1479747893093)),
    new CityWeather("Balaklava", new OutdoorData(WeatherIcon.Snow, 1, 1004, 10, 5, 1479747893093)),
    new CityWeather("Antwerp", new OutdoorData(WeatherIcon.PartlyCloudyNight, 16, 1004, 10, 5, 1479747893093))
  ];

  indoorData: IndoorData = new IndoorData(21.3, 0.43, 1479747893093);
  indoorHistoryChartData: c3.Data = this.prepareIndoorHistoryChartData([]);


  constructor(private dataService: DataService) {

  }


  currentTime: Date = new Date(Date.now());

  prepareTemperatureForecastChartData(data: OutdoorData[]): c3.Data {
    let dates: Array<any> = data.map(forecast => new Date(forecast.time));
    let temperatures: Array<any> = data.map(forecast => forecast.temperature.toFixed(1));
    dates.unshift('datetime');
    temperatures.unshift('temperature');

    return {
      columns: [
        dates,
        temperatures
      ],
      colors: {
        temperature: '#ff6400',
      },
      x: 'datetime',
      types: {
        temperature: 'area-step'
      },
      labels: true
    }
  }

  preparePrecipitationForecastChartData(data: OutdoorData[]): c3.Data {
    let dates: Array<any> = data.map(forecast => new Date(forecast.time));
    let precipitationProbabilities: Array<any> = data.map(forecast => forecast.precipitationProbability);
    dates.unshift('datetime');
    precipitationProbabilities.unshift('probability');

    return {
      columns: [
        dates,
        precipitationProbabilities,
        // ['intensity', 0.1, 0.3, 0, 1, 0.3, 0.5]
      ],
      colors: {
        intensity: '#779ecb',
      },
      x: 'datetime',
      types: {
        probability: 'area-step',
        // intensity: 'area-step',
      },
      labels: true
    }
  }

  private prepareIndoorHistoryChartData(indoorHistoryData: IndoorData[]): c3.Data {
    let dates: Array<any> = indoorHistoryData.map(data => new Date(data.date));
    let temperatures: Array<any> = indoorHistoryData.map(data => data.temperature);
    let humidity: Array<any> = indoorHistoryData.map(data => data.humidity);
    dates.unshift('datetime');
    temperatures.unshift('temperature');
    humidity.unshift('humidity');

    return {
      columns: [
        dates,
        temperatures,
        humidity
      ],
      types: {
        temperature: 'area-spline',
        humidity: 'area-spline'
      },
      x: 'datetime',
      colors: {
        temperature: '#ff6400',
        humidity: '#779ecb'
      },
      axes: {
        temperature: 'y',
        humidity: 'y2'
      },
      labels: true
    }
  }
}

class CityWeather {

  constructor(public cityName: string,
              public weather: OutdoorData) {
  }
}

class TimedData<T> {

  constructor(public date: Date, public data: T) {
  }
}


class Settings {
  public indoorTemperatureColors: any = {0: '#779ECB', 18: '#60B044', 24: '#F6C600', 28: '#FF0000'};
  public indoorHumidityColors:any = {0: '#CCCCCC', 20: '#A3B6CC', 40: '#779ecb', 60: '#5089CC', 80: '#397DCC'};

}
