import {Component, Input, SimpleChange} from '@angular/core';
import {WeatherIcon} from "../service/data";

@Component({
  moduleId: module.id,
  selector: 'wth-weather-icon',
  templateUrl: 'weather-icon.component.html',
  styleUrls: ['weather-icon.component.css']
})
export class WeatherIconComponent {

  @Input() icon: WeatherIcon;
  iconPath: string;

  private iconMapping: Map<WeatherIcon, string> = new Map<WeatherIcon, string>();

  getIconPath(icon: WeatherIcon): string {
    return this.iconMapping.get(icon);
  }

  ngOnChanges(changes: {[value: string]: SimpleChange}) {
    if (changes['icon']) {
      this.iconPath = this.getIconPath(this.icon);
      console.log("icon icon: ", this.icon);
      console.log("icon mapped: ", this.iconPath);
    }
  }

  constructor() {

    this.iconMapping.set(WeatherIcon.ClearDay, "/icons/clear-day.svg");
    this.iconMapping.set(WeatherIcon.ClearNight, "/icons/clear-night.svg");
    this.iconMapping.set(WeatherIcon.Rain, "/icons/rain.svg");
    this.iconMapping.set(WeatherIcon.Snow, "/icons/snow.svg");
    this.iconMapping.set(WeatherIcon.Sleet, "/icons/sleet.svg");
    this.iconMapping.set(WeatherIcon.Wind, "/icons/wind.svg");
    this.iconMapping.set(WeatherIcon.Fog, "/icons/fog.svg");
    this.iconMapping.set(WeatherIcon.Cloudy, "/icons/cloudy.svg");
    this.iconMapping.set(WeatherIcon.PartlyCloudyDay, "/icons/partly-cloudy-day.svg");
    this.iconMapping.set(WeatherIcon.PartlyCloudyNight, "/icons/partly-cloudy-night.svg");
    this.iconMapping.set(WeatherIcon.Other, "/icons/other.svg");
  }
}
