/***********************************************************************************************
 * User Configuration.
 **********************************************************************************************/
/** Map relative paths to URLs. */
const map: any = {
  'c3': 'vendor/c3',
  'd3': 'vendor/c3/node_modules/d3',
  'moment': 'vendor/moment',
};

/** User packages configuration. */
const packages: any = {
  'c3': {
    format: 'cjs',
    defaultExtension: 'js',
    main: 'c3.js'
  },
  'd3': {
    format: 'cjs',
    defaultExtension: 'js',
    main: 'd3.js'
  },
  'moment': {
    format: 'cjs',
    main: 'moment.js',
    defaultExtension: 'js'
  }
};

////////////////////////////////////////////////////////////////////////////////////////////////
/***********************************************************************************************
 * Everything underneath this line is managed by the CLI.
 **********************************************************************************************/
const barrels: string[] = [
  // Angular specific barrels.
  '@angular/core',
  '@angular/common',
  '@angular/compiler',
  '@angular/http',
  '@angular/router',
  '@angular/platform-browser',
  '@angular/platform-browser-dynamic',

  // Thirdparty barrels.
  'rxjs',

  // App specific barrels.
  'app',
  'app/shared',
  'app/left-panel',
  'app/weather-icon',
  'app/outdoor-weather',
  'app/charts/step-chart',
  'app/city-weather',
  'app/charts/gauge-chart',
  'app/charts/area-chart',
  'app/clock',
  /** @cli-barrel */
];

const cliSystemConfigPackages: any = {};
barrels.forEach((barrelName: string) => {
  cliSystemConfigPackages[barrelName] = { main: 'index' };
});

/** Type declaration for ambient System. */
declare var System: any;

// Apply the CLI SystemJS configuration.
System.config({
  map: {
    '@angular': 'vendor/@angular',
    'rxjs': 'vendor/rxjs',
    'main': 'main.js'
  },
  packages: cliSystemConfigPackages
});

// Apply the user's configuration.
System.config({ map, packages });
